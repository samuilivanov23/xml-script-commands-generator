import os
from . import db
from lxml import etree
from .utils import Utils
from xml.dom import minidom
import xml.etree.ElementTree as ET
from flask_login import login_required, current_user
from flask import Blueprint, render_template, url_for, redirect, request, flash

XML_IDENTATION = "      "
XML_RESULT_FILE_PATH_SHELL = os.getcwd() + '/project/xml_result_files/xml_commands_sh_script.xml'
XML_RESULT_FILE_PATH_SYSTEMD = os.getcwd() + '/project/xml_result_files/systemd_configuration.xml'
SYSTEMD_RESULT_FILE_PATH = os.getcwd() + '/project/systemd_result_files/'
XSD_SHELL_FILE_PATH = os.getcwd() + '/project/xsd_schemas/sh_script_commands_schema.xsd'
XSD_SYSTEMD_FILE_PATH = os.getcwd() + '/project/xsd_schemas/systemd_configuration_schema.xsd'
XSD_SCHEMA_SHELL = etree.XMLSchema(file=XSD_SHELL_FILE_PATH)
XSD_SCHEMA_SYSTEMD = etree.XMLSchema(file=XSD_SYSTEMD_FILE_PATH)

main = Blueprint('main', __name__)

@main.route('/')
def index():
    return render_template('index.html')

@main.route('/profile')
@login_required
def profile():
    return render_template('profile.html', name=current_user.name)

@main.route('/buildscript')
@login_required
def buildscript():
    return render_template('buildscript.html')

@main.route('/buildscript', methods=['POST'])
@login_required
def buildscript_post():
    commands = [request.form[form_parameter] for form_parameter in request.form if request.form[form_parameter]]
    root = ET.Element("commands")

    for command in commands:
        ET.SubElement(root, "command").text = command

    # Create a string with proper indentation
    xml_string = ET.tostring(root, encoding='utf-8', method='xml')
    formatted_xml = minidom.parseString(xml_string).toprettyxml(indent=XML_IDENTATION)
    xml_content = etree.fromstring(formatted_xml)

    if not Utils.validate_xml(xml_content, XSD_SCHEMA_SHELL):
        return render_template('buildscript.html')
    else:
        with open(XML_RESULT_FILE_PATH_SHELL, "w") as xml_file:
            xml_file.write(formatted_xml)

    return redirect(url_for('main.profile'))

@main.route('/buildsystemd')
@login_required
def buildsystemd():
    return render_template('buildsystemd.html')

@main.route('/buildsystemd', methods=['POST'])
@login_required
def buildsystemd_post():
    Utils.parse_systemd_form_data(request.form)
    root = ET.Element("systemdConfig")
    unit_type = ET.SubElement(root, "service")
    file_name = ET.SubElement(unit_type, "FileName").text = request.form["FileName"]
    config = ET.SubElement(unit_type, "config")

    for section_name, section_data in Utils.systemd_service_unit_sections_data["sections_data"].items():
        section_element = ET.SubElement(config, section_name)

        for key, value in section_data.items():
            subelement = ET.SubElement(section_element, key).text = value
    
    xml_string = ET.tostring(root, encoding='utf-8', method='xml')
    formatted_xml = minidom.parseString(xml_string).toprettyxml(indent=XML_IDENTATION)
    xml_content = etree.fromstring(formatted_xml)

    if not Utils.validate_xml(xml_content, XSD_SCHEMA_SYSTEMD):
        return render_template('buildsystemd.html')
    else:
        with open(XML_RESULT_FILE_PATH_SYSTEMD, "w") as xml_file:
            xml_file.write(formatted_xml)
        
        unit_type = "service"
        Utils.generate_systemd_file(SYSTEMD_RESULT_FILE_PATH, file_name, unit_type, Utils.systemd_service_unit_sections_data["sections_data"])

    return redirect(url_for('main.profile'))