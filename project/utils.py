from lxml import etree
from flask import flash

class Utils:
    UNIT_SECTION_PREFIX_IN_FORM = "Unit_"
    SERVICE_SECTION_PREFIX_IN_FORM = "Service_"
    INSTALL_SECTION_PREFIX_IN_FORM = "Install_"

    systemd_service_unit_sections_data = {
        "sections_structute_prefixes_in_form":{
            "Unit":"Unit_",
            "Service":"Service_",
            "Install":"Install_"
        },
        "sections_data":{
            "Unit":{},
            "Service":{},
            "Install":{}
        }
    }

    @staticmethod
    def validate_xml(xml_content, xsd_schema):
        try:
            xsd_schema.assertValid(xml_content)
        except etree.DocumentInvalid as e:
            print(f'Error: {e}')
            flash(f'XML validation failed!\nError: {e}')
            return False
        
        return True
    
    @staticmethod
    def parse_systemd_form_data(request_form):
        try:
            for systemd_timer_sections_prefix in Utils.systemd_service_unit_sections_data["sections_structute_prefixes_in_form"]:
                Utils.systemd_service_unit_sections_data["sections_data"][systemd_timer_sections_prefix] = Utils.extract_systemd_section_data(request_form, 
                                                                                    Utils.systemd_service_unit_sections_data["sections_structute_prefixes_in_form"][systemd_timer_sections_prefix])
        except Exception as e:
            print(f"{e}.")

    @staticmethod
    def extract_systemd_section_data(request_form, section_prefix):
        return {k.replace(section_prefix, ""): v for k, v in request_form.items() if k.startswith(section_prefix)}
    
    @staticmethod
    def generate_systemd_file(SYSTEMD_RESULT_FILE_PATH, file_name, unit_type, systemd_unit_file_data):
        try:
            full_file_path = SYSTEMD_RESULT_FILE_PATH + file_name + '.' + unit_type
            with open(full_file_path, "w") as systemd_file:
                for section, values in systemd_unit_file_data.items():
                    systemd_file.write(f"[{section}]\n")
                    for key, value in values.items():
                        systemd_file.write(f"{key}={value}\n")
                    systemd_file.write("\n")
        except Exception as e:
            print(f"{e}.")